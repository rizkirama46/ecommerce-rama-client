import './App.css';
import { CartProvider } from './Context/CartContext';
import { UserProvider } from './Context/UserContext';
import Routes from './Routes';

function App() {
  return (
    <>
      <UserProvider>
        <CartProvider>
          <Routes />
        </CartProvider>
      </UserProvider>
    </>
  );
}

export default App;
