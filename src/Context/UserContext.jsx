import React, { createContext } from 'react'

export const UserContext = createContext()

export const UserProvider = ({ children }) => {
  const currentUser = JSON.parse(localStorage.getItem('user'))

  const [user, setUser] = React.useState(currentUser)
  
  return (
    <UserContext.Provider value={[user, setUser]}>
      {children}
    </UserContext.Provider>
  )
}
