import axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import { Link, useNavigate, useParams } from 'react-router-dom'
import AdminSideBar from '../../Components/AdminSideBar'
import { UserContext } from '../../Context/UserContext'

export default function FormCategory() {
  const { id } = useParams()
  const api = `https://ecommerce-app-rnp.herokuapp.com`
  const history = useNavigate()
  const [user] = useContext(UserContext)

  const formInput = {
    "product_name": "",
  }

  const [input, setInput] = useState(formInput)

  useEffect(() => {
    const fetchData = async () => {
      if(id) {
        const res = await axios.get(`${api}/categories/${id}`, {headers: { Authorization: `Bearer ${user.token}` }})
        setInput(res.data.data)
      }
    }

    fetchData()
  }, [id, api, user])

  const handleInputChange = (event) => {
    const { name, value } = event.target
    setInput({ ...input, [name]: value })
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    try {
      if(id) {
        const res = await axios.put(`${api}/categories/${id}`, input, {headers: { Authorization: `Bearer ${user.token}` }})
        alert(res.data.message)
      } else {
        const res = await axios.post(`${api}/categories`, input, {headers: { Authorization: `Bearer ${user.token}` }})
        alert(res.data.message)
      }
      history('/admin/categories')
    } catch (error) {
      alert(JSON.stringify(error.response.data.message))
    }
  }
  return (
    <div>
      <AdminSideBar />
      <div className="absolute top-24 left-64 w-4/5">
        <div className='px-5'>
          <h1 className='text-2xl font-bold'>Form Product</h1>
        </div>
        <div className='p-5'>
          <form onSubmit={handleSubmit}>
            <div className="form-group mb-6">
              <label htmlFor="category_name" className="form-label inline-block mb-2 text-slate-900">Product Name</label>
              <input type="text" name='category_name' value={input.category_name}  id="category_name" placeholder="Enter category name" onChange={handleInputChange} className="form-control w-full px-3 py-1.5 border border-solid border-gray-300rounded focus:bg-white focus:border-blue-600 focus:outline-none" />
            </div>
            <div className="flex gap-x-2">
              <Link to="/admin/categories" type="submit" className="px-6 py-2.5 bg-slate-900 text-white font-medium text-xs uppercase rounded shadow-md hover:opacity-80">back</Link>
              <button type="submit" className="px-6 py-2.5 bg-slate-900 text-white font-medium text-xs uppercase rounded shadow-md hover:opacity-80">submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}
