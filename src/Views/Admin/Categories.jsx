import axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import AdminSideBar from '../../Components/AdminSideBar'
import { UserContext } from '../../Context/UserContext'

export default function Categories() {
  const api = `https://ecommerce-app-rnp.herokuapp.com/categories`
  const [categories, setCategories] = useState([])
  const [user] = useContext(UserContext)
  const [trigger, setTrigger] = useState(true)

  useEffect(() => {
    const fetchData = async () => {
      const res = await axios.get(api)

      setCategories(res.data.data.map(category => {
        return {
          id: category.category_id,
          name: category.category_name,
        }
      }))

    }
    if(trigger) {
      fetchData()
      setTrigger(false)
    }
  }, [api, trigger])

  const handleDelete = async (id) => {
    try {
      const res = await axios.delete(`${api}/${id}`, {headers: { Authorization: `Bearer ${user.token}` }})
      alert(res.data.message)
      setTrigger(true)
    } catch (error) {
      alert(JSON.stringify(error.response.data.message))
    }
  }

  return (
    <div className='flex mt-32'>
      <div className="w-1/6">
        <AdminSideBar />
      </div>
      <div className="w-5/6">
        <div className='px-5'>
          <div>
            <h1 className='text-2xl font-bold'>Products</h1>
          </div>
          <div className="mt-5">
            <Link to='/admin/category/create' className='px-4 py-2 bg-slate-800 text-slate-100 hover:opacity-80'>
              Add Categories
            </Link>
          </div>
          <div className="relative overflow-x-auto shadow-md sm:rounded-md mt-5">
            <table className="w-full text-sm text-left">
              <thead className="text-xs text-gray-700 uppercase bg-gray-50">
                <tr>
                  <th scope="col" className="px-6 py-3 w-1/3">
                    Product name
                  </th>
                  <th scope="col" className="px-6 py-3">
                    action
                  </th>
                </tr>
              </thead>
              <tbody>
                { categories?.map(categories => {
                  return (
                    <tr className="bg-white border-b">
                      <th scope="row" className="px-6 py-4 font-medium">
                        {categories.name}
                      </th>
                      <td className="px-6 py-4">
                        <Link to={`/admin/category/${categories.id}/edit`} className='px-4 py-2 bg-slate-800 text-slate-100 hover:opacity-80 mr-2'>Edit</Link>
                        <button onClick={()=> handleDelete(categories.id)} className='px-4 py-2 bg-slate-800 text-slate-100 hover:opacity-80'>Hapus</button>
                      </td>
                    </tr>
                  )
                })}

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
}
