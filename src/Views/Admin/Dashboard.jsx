import React from 'react'
import AdminSideBar from '../../Components/AdminSideBar'

export default function Dashboard() {
  return (
    <div>
      <AdminSideBar />
      <div className="absolute top-24 left-64 w-full">
        <div className='px-5'>
          <h1 className='text-2xl font-bold'>Dashboard</h1>
        </div>
      </div>
    </div>
  )
}
