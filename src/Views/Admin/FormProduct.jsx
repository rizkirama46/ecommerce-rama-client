import axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import { Link, useNavigate, useParams } from 'react-router-dom'
import AdminSideBar from '../../Components/AdminSideBar'
import { UserContext } from '../../Context/UserContext'

export default function FormProduct() {
  const { id } = useParams()
  const api = `https://ecommerce-app-rnp.herokuapp.com`
  const history = useNavigate()
  const [user] = useContext(UserContext)

  const formInput = {
    "category_id": 0,
    "description": "",
    "image_url": "",
    "price": 0,
    "product_name": "",
    "stock": 0,
    "type": ""
  }

  const [input, setInput] = useState(formInput)
  const [categories, setCategories] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      if(id) {
        const res = await axios.get(`${api}/products/${id}`, {headers: { Authorization: `Bearer ${user.token}` }})
        setInput(res.data.data)
      }
      const res = await axios.get(`${api}/categories`)
      setCategories(res.data.data.map(category => {
        return{
          id: category.category_id,
          name: category.category_name,
        }
      })) 
    }

    fetchData()
  }, [id, api, user])

  const handleInputChange = (event) => {
    const { name, value } = event.target
    console.log(name, value);
    setInput({ ...input, [name]: value })
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    try {
      input.category_id = +input.category_id
      input.price = parseFloat(input.price)
      input.stock = +input.stock 
      if(id) {
        const res = await axios.put(`${api}/products/${id}`, input, {headers: { Authorization: `Bearer ${user.token}` }})
        alert(res.data.message)
      } else {
        const res = await axios.post(`${api}/products`, input, {headers: { Authorization: `Bearer ${user.token}` }})
        alert(res.data.message)
      }
      history('/admin/products')
    } catch (error) {
      alert(JSON.stringify(error.response.data.message))
    }
  }
  return (
    <div>
      <AdminSideBar />
      <div className="absolute top-24 left-64 w-4/5">
        <div className='px-5'>
          <h1 className='text-2xl font-bold'>Form Product</h1>
        </div>
        <div className='p-5'>
          <form onSubmit={handleSubmit}>
            <div className="form-group mb-6">
              <label htmlFor="product_name" className="form-label inline-block mb-2 text-slate-900">Product Name</label>
              <input type="text" name='product_name' value={input.product_name}  id="product_name" placeholder="Enter product name" onChange={handleInputChange} className="form-control w-full px-3 py-1.5 border border-solid border-gray-300rounded focus:bg-white focus:border-blue-600 focus:outline-none" />
            </div>
            <div className="form-group mb-6">
              <label htmlFor="category_id" className="form-label inline-block mb-2 text-slate-900">Category</label>
              <select id="category_id" name='category_id' onChange={handleInputChange} class="bg-gray-50 border border-gray-300 text-gray-900 text-sm  focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                <option selected>Choose a country</option>
                {categories.map(category => {
                  return <option value={category.id}>{category.name}</option>
                })}
              </select>
            </div>
            <div className="form-group mb-6">
              <label htmlFor="price" className="form-label inline-block mb-2 text-slate-900">Price</label>
              <input type="number" name='price' value={input.price}  id="price" placeholder="Enter price" onChange={handleInputChange} className="form-control w-full px-3 py-1.5 border border-solid border-gray-300rounded focus:bg-white focus:border-blue-600 focus:outline-none" />
            </div>
            <div className="form-group mb-6">
              <label htmlFor="image_url" className="form-label inline-block mb-2 text-slate-900">Image Url</label>
              <input type="text" name='image_url' value={input.image_url}  id="image_url" placeholder="Enter image url" onChange={handleInputChange} className="form-control w-full px-3 py-1.5 border border-solid border-gray-300rounded focus:bg-white focus:border-blue-600 focus:outline-none" />
            </div>
            <div className="form-group mb-6">
              <label htmlFor="stock" className="form-label inline-block mb-2 text-slate-900">Stock</label>
              <input type="number" name='stock' value={input.stock}  id="stock" placeholder="Enter stock" onChange={handleInputChange} className="form-control w-full px-3 py-1.5 border border-solid border-gray-300rounded focus:bg-white focus:border-blue-600 focus:outline-none" />
            </div>
            <div className="form-group mb-6">
              <label htmlFor="type" className="form-label inline-block mb-2 text-slate-900">type</label>
              <input type="text" name='type' value={input.type}  id="type" placeholder="Enter type" onChange={handleInputChange} className="form-control w-full px-3 py-1.5 border border-solid border-gray-300rounded focus:bg-white focus:border-blue-600 focus:outline-none" />
            </div>
            <div className="form-group mb-6">
              <label htmlFor="description" className="form-label inline-block mb-2 text-slate-900">Description</label>
              <textarea name='description' value={input.description} id="description" rows="3" placeholder="description" onChange={handleInputChange} className="form-control block w-full px-3 py-1.5 text-slate-900 bg-white border rounded focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"></textarea>
            </div>
            <div className="flex gap-x-2">
              <Link to="/admin/products" type="submit" className="px-6 py-2.5 bg-slate-900 text-white font-medium text-xs uppercase rounded shadow-md hover:opacity-80">back</Link>
              <button type="submit" className="px-6 py-2.5 bg-slate-900 text-white font-medium text-xs uppercase rounded shadow-md hover:opacity-80">submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}
