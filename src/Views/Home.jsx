import React from 'react'
import HomeBestSeller from '../Components/HomeBestSeller'
import Footer from '../Components/Footer'
import Hero from '../Components/Hero'
import HomeCollection from '../Components/HomeCollection'

export default function Home() {
  return (
    <div className='pt-16'>
      <Hero />
      <div className="container py-5 my-10 md:px-10 lg:px-28">
        <div className="flex flex-wrap">
          <div className="flex items-center w-1/2 md:w-1/4">
            <svg xmlns="http://www.w3.org/2000/svg" className="h-16 w-16 text-slate-900" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
              <path strokeLinecap="round" strokeLinejoin="round" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
              <path strokeLinecap="round" strokeLinejoin="round" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
            </svg>
            <h3 className='text-md font-medium ml-2'>JAMINAN KUALIATAS PRODUK</h3>
          </div>
          <div className="flex items-center w-1/2 md:w-1/4">
            <svg xmlns="http://www.w3.org/2000/svg" className="h-16 w-16 text-slate-900" viewBox="0 0 20 20" fill="currentColor">
              <path fillRule="evenodd" d="M4 2a1 1 0 011 1v2.101a7.002 7.002 0 0111.601 2.566 1 1 0 11-1.885.666A5.002 5.002 0 005.999 7H9a1 1 0 010 2H4a1 1 0 01-1-1V3a1 1 0 011-1zm.008 9.057a1 1 0 011.276.61A5.002 5.002 0 0014.001 13H11a1 1 0 110-2h5a1 1 0 011 1v5a1 1 0 11-2 0v-2.101a7.002 7.002 0 01-11.601-2.566 1 1 0 01.61-1.276z" clipRule="evenodd" />
            </svg>
            <h3 className='text-md font-medium ml-3'>7-DAYS RETURN & EXCHANGE</h3>
          </div>
          <div className="flex items-center w-1/2 md:w-1/4">
            <svg xmlns="http://www.w3.org/2000/svg" className="h-16 w-16 text-slate-900" viewBox="0 0 20 20" fill="currentColor">
              <path fillRule="evenodd" d="M5 5a3 3 0 015-2.236A3 3 0 0114.83 6H16a2 2 0 110 4h-5V9a1 1 0 10-2 0v1H4a2 2 0 110-4h1.17C5.06 5.687 5 5.35 5 5zm4 1V5a1 1 0 10-1 1h1zm3 0a1 1 0 10-1-1v1h1z" clipRule="evenodd" />
              <path d="M9 11H3v5a2 2 0 002 2h4v-7zM11 18h4a2 2 0 002-2v-5h-6v7z" />
            </svg>
            <h3 className='text-md font-medium ml-3'>FAST SHIPPING</h3>
          </div>
          <div className="flex items-center w-1/2 md:w-1/4">
            <svg xmlns="http://www.w3.org/2000/svg" className="h-16 w-16 text-slate-900" viewBox="0 0 20 20" fill="currentColor">
              <path fillRule="evenodd" d="M4 4a2 2 0 00-2 2v4a2 2 0 002 2V6h10a2 2 0 00-2-2H4zm2 6a2 2 0 012-2h8a2 2 0 012 2v4a2 2 0 01-2 2H8a2 2 0 01-2-2v-4zm6 4a2 2 0 100-4 2 2 0 000 4z" clipRule="evenodd" />
            </svg>
            <h3 className='text-md font-medium ml-3'>GARANSI UANG KEMBALI</h3>
          </div>
        </div>
      </div>
      <HomeBestSeller />
      <HomeCollection />
      <Footer />
    </div>
  )
}
