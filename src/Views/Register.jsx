import React, { useState } from 'react'
import Footer from '../Components/Footer'
import { useNavigate } from "react-router-dom"
import axios from 'axios'

export default function Register() {
  const history = useNavigate()
  const api = `https://ecommerce-app-superbootcamp.herokuapp.com/register`
  const formInput = {
    "address": "",
    "email": "",
    "name": "",
    "password": "",
    "phone": ""
  }

  const [input, setInput] = useState(formInput)

  const handleInputChange = (event) => {
    const { name, value } = event.target
    setInput({ ...input, [name]: value })
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    try {
      const post = await axios.post(api, input)
      if(post.data.success === true) {
        alert('Register Success')
        history('/login')
      }
    } catch (error) {
      alert(JSON.stringify(error.response.data.error))
    }
  }

  return (
    <>
      <div className="flex justify-center mt-36 pt-16 pb-52">
        <div className="w-full md:w-1/3 flex flex-col items-center">
          <h1 className='font-semibold text-xl'>REGISTER</h1>
          <p className='text-md my-5'>Please fill in the information below : </p>
          <form onSubmit={handleSubmit}>
            <input type="text" name='name' placeholder='Your Name' className='w-full border p-2 my-2' value={input.name} onChange={handleInputChange} />
            <input type="email" name='email' placeholder='Email' className='w-full border p-2 my-2' value={input.email} onChange={handleInputChange} />
            <input type="password" name='password' placeholder='Password' className='w-full border p-2 my-2' value={input.password} onChange={handleInputChange} />
            <input type="text" name='phone' placeholder='Phone Number' className='w-full border p-2 my-2' value={input.phone} onChange={handleInputChange} />
            <textarea name="address" rows="4" placeholder='Your Address' className='w-full border p-2 my-2' value={input.address} onChange={handleInputChange}></textarea>
            <button type='submit' className='w-full bg-slate-900 text-white py-3 my-2 hover:text-slate-900 hover:bg-white hover:border hover:border-slate-900 transition duration-500'>CREATE MY ACCOUNT</button>
          </form>
        </div>
      </div>
      <Footer />
    </>
  )
}
