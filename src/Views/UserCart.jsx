import axios from 'axios'
import React, { useContext, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { CartContext } from '../Context/CartContext'
import { UserContext } from '../Context/UserContext'

export default function UserCart() {
  const api = 'https://ecommerce-app-rnp.herokuapp.com'
  const history = useNavigate()
  const [cart, setCart] = useContext(CartContext)
  const [user, setUser] = useContext(UserContext)
  let total = 0

  cart.forEach(e => {
    total += e.quantity * e.Product.price
  })

  const initialInput = {
    "cart_id": 0,
    "quantity": 0
  }
  const [input, setInput] = useState(initialInput)

  const handleQuantity = async (event, type, cartID, quantity, productStock) => {
    event.preventDefault()
    try {
      if (type === 'plus') {
        if(quantity === productStock) throw new Error('Product is out of stock')
        initialInput.cart_id = cartID
        initialInput.quantity = quantity + 1
        await axios.put(`${api}/carts/${cartID}`, initialInput, {headers: {'Authorization': `Bearer ${user.token}`}})
        setCart(cart.map(item => item.cart_id === cartID ? {...item, quantity: quantity + 1} : item))
      } else {
        if(quantity < 1) throw new Error('Minimum quantity is 1')
        initialInput.cart_id = cartID
        initialInput.quantity = quantity - 1
        await axios.put(`${api}/carts/${cartID}`, initialInput, {headers: {'Authorization': `Bearer ${user.token}`}})
        setCart(cart.map(item => item.cart_id === cartID ? {...item, quantity: quantity - 1} : item))
      }     
    } catch (error) {
      alert(JSON.stringify(error.response.data))
    }
  }

  const handleInputChange = (event) => {
    const { name, value } = event.target
    setInput({ ...input, [name]: value })
  }

  const handleDeleteCart = async (event, id) => {
    event.preventDefault()

    try {
      await axios.delete(`${api}/carts/${id}`, {headers: {"Authorization" : "Bearer "+ user.token}})
      alert('Delete cart success')
      setCart(cart.filter(item => item.cart_id !== id))
    } catch (error) {
      alert(JSON.stringify(error.response.data))
    }
  }
  
  const handleCheckout = async (event) => {
    event.preventDefault()
    try {
      await axios.patch(`${api}/transactions/checkout`, {total}, {headers: {"Authorization" : "Bearer "+ user.token}})
      setUser({ ...user, balance: parseFloat(user.balance) - parseFloat(total) })
      setCart([])
      alert('Checkout success')
      history('/user/order')
    } catch (error) {
      alert(JSON.stringify(error.response.data.message))
    }
  }

  return (
    <div className='relative top-32'>
      <div className='flex justify-center mb-10'>
        <h1 className='font-medium text-5xl'>CARTS</h1>
      </div>
      <div className='container'>
        <div className="px-5">
          <div className="flex py-4 px-8 items-center gap-5 bg-white rounded-sm shadow-md">
            <div className="flex w-2/5">
              <p>Product</p>
            </div>
            <p className='w-1/6'>Harga</p>
            <p className='w-1/5'>Kuantitas</p>
            <p className='w-1/6'>Total Harga</p>
            <p>Action</p>
          </div>
          { cart.map( cart => {
            return (
              <div key={cart.cart_id} className="flex py-8 px-8 items-center gap-5 mt-5 bg-white rounded-sm shadow-md">
                <div className="flex w-2/5">
                  <Link to={`/product/${cart.Product.product_id}/detail`} className="flex items-center w-full">
                    <img className='h-20 w-20' src={cart.Product.image_url} alt="productCart" />
                    <p className='mx-3 font-semibold text-sm w-full'>{cart.Product.product_name}</p>
                    <p className='w-1/5 capitalize'>{cart.Product.type}</p>
                  </Link>
                </div>
                <p className='flex justify-center w-1/6'>IDR {cart.Product.price}</p>
                <div className='flex justify-center w-1/5'>
                  <button onClick={(e)=> handleQuantity(e, "minus", cart.cart_id, cart.quantity)} className='w-1/6 border bg-slate-50 my-2'>
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 m-auto" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                      <path strokeLinecap="round" strokeLinejoin="round" d="M18 12H6" />
                    </svg>
                  </button>
                  <input type="text" name='quantity' value={cart.quantity} className='w-1/3 px-6 focus:outline-none' onChange={handleInputChange}/>
                  <button onClick={(e)=> handleQuantity(e, "plus", cart.cart_id, cart.quantity, cart.Product.stock)} className='w-1/6 border bg-slate-50 my-2'>
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 m-auto" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                      <path strokeLinecap="round" strokeLinejoin="round" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                    </svg>
                  </button>
                </div>
                <p className='flex justify-center w-1/6 text-slate-800 font-semibold'>IDR {cart.quantity * cart.Product.price}</p>
                <button onClick={(e)=> handleDeleteCart(e, cart.cart_id)} className='py-2 px-4 bg-slate-900 hover:opacity-80 text-slate-100'>Hapus</button>
              </div> 
            )
          })}
        </div>
      </div>
      <div className="mb-72"></div>
      <div className='fixed bottom-0 bg-white left-9 right-9 h-28 px-10'>
        <div className='flex items-center justify-end gap-x-10'>
          <div className='text-xl flex flex-row gap-x-3'>
            <p className='text-xl font-semibold'>Total Product({cart?.length}): IDR {total} </p>
          </div>
          <button onClick={handleCheckout} className='uppercase text-sm font-semibold px-10 py-2 bg-slate-900 text-slate-300 hover:opacity-80'>Checkout</button>
        </div>
      </div>
    </div>
  )
}
