import axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import Footer from '../Components/Footer'
import UserSideBar from '../Components/UserSideBar'
import { UserContext } from '../Context/UserContext'

export default function UserProfile() {
  const api = 'https://ecommerce-app-rnp.herokuapp.com'
  const [user, setUser] = useContext(UserContext)
  const [trigger, setTrigger] = useState(true)

  const formInput = {
    "nama": "",
    "address": "",
    "phone": "",
  }

  const [input, setInput] = useState(formInput)

  useEffect(() => {
    const fetchData = async () => {
      const response = await axios.get(`${api}/users`, {headers: { Authorization: `Bearer ${user.token}` }})
      setInput(response.data.data)
    }

    if(trigger) {
      fetchData()
      setTrigger(false)
    }
  }, [user, trigger])

  const handleInputChange = (event) => {
    const { name, value } = event.target
    setInput({ ...input, [name]: value })
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    try {
      await axios.put(`${api}/users`, input, {headers: { Authorization: `Bearer ${user.token}` }})
      setUser(null)
      alert('Profile updated')
    } catch (error) {
      alert(JSON.stringify(error.response.data.error))
    }
  }

  return (
    <>
      <div className="container">
        <div className='relative px-8 top-32'>
          <div className='flex justify-center mb-10'>
            <h1 className='font-medium text-5xl'>USER PROFILE</h1>
          </div>
          <div className="flex">
            <div className="w-1/5">
              <UserSideBar />
            </div>
            <div className="w-4/5 p-5 shadow-sm bg-white">
              <form onSubmit={handleSubmit}>
                <div className="form-group mb-6">
                  <label htmlFor="name" className="form-label inline-block mb-2 text-slate-900">Fullname</label>
                  <input type="text" name='name' value={input.name} onChange={handleInputChange} className="form-control w-full px-3 py-1.5 border border-solid border-gray-300rounded focus:bg-white focus:border-blue-600 focus:outline-none" id="name"placeholder="Enter fullname" />
                </div>
                <div className="form-group mb-6">
                  <label htmlFor="email" className="form-label inline-block mb-2 text-slate-900">Email address</label>
                  <input type="email" name='email' value={input.email} onChange={handleInputChange} className="form-control w-full px-3 py-1.5 border border-solid border-gray-300rounded focus:bg-white focus:border-blue-600 focus:outline-none" id="email"placeholder="Enter email" disabled/>
                </div>
                <div className="form-group mb-6">
                  <label htmlFor="phone" className="form-label inline-block mb-2 text-slate-900">Phone Number</label>
                  <input type="text" name='phone' value={input.phone} onChange={handleInputChange} className="form-control w-full px-3 py-1.5 border border-solid border-gray-300rounded focus:bg-white focus:border-blue-600 focus:outline-none" id="phone"placeholder="Enter phone number" />
                </div>
                <div className="form-group mb-6">
                  <label htmlFor="address" className="form-label inline-block mb-2 text-slate-900">Address</label>
                  <textarea name='address' value={input.address} onChange={handleInputChange} className="form-control block w-full px-3 py-1.5 text-slate-900 bg-white border rounded focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" id="address" rows="3" placeholder="Address"></textarea>
                </div>
                <button className="px-6 py-2.5 bg-slate-900 text-white font-medium text-xs uppercase rounded shadow-md">Update</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div className="mb-64"></div>
      <Footer />
    </>
  )
}
