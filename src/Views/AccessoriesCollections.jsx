import React from 'react'
import BaseCollection from '../Components/Collection/BaseCollection'

export default function AccessoriesCollections() {
  return (
    <div>
      <BaseCollection 
        page="accessories" 
        title="ACCESSORIES COLLECTIONS" 
        api="https://ecommerce-app-rnp.herokuapp.com/products/accessories"/>
    </div>
  )
}
