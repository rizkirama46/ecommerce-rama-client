import axios from 'axios'
import { useContext, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../Components/Footer'
import UserSideBar from '../Components/UserSideBar'
import { UserContext } from '../Context/UserContext'

export default function Order() {
  const api = 'https://ecommerce-app-rnp.herokuapp.com/transactions'
  const [user] = useContext(UserContext)
  const [transactions, setTransactions] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      const response = await axios.get(api, {headers: {Authorization: `Bearer ${user.token}`}})

      setTransactions(response.data.data.map(transaction => {
        return {
          id: transaction.transaction_id,
          quantity: transaction.quantity,
          user_id: transaction.user_id,
          Product: {
            id: transaction.Product.product_id,
            name: transaction.Product.product_name,
            price: transaction.Product.price,
            image: transaction.Product.image_url,
            type: transaction.Product.type
          }
        }
      }))
    }

    fetchData()
  }, [user])

  return (
    <>
      <div className="container">
        <div className="relative px-8 top-32">
          <div className='flex justify-center mb-10'>
            <h1 className='font-medium text-5xl'>HISTORY ORDERS</h1>
          </div>
          <div className="flex">
            <div className="w-1/5">
              <UserSideBar />
            </div>
            <div className="w-4/5">
              <div className="flex flex-wrap gap-10">
                { transactions.map(transaction => {
                  return (
                    <div className='w-full shadow-md rounded-sm p-4 bg-white' key={transaction.id}>
                      <div className="flex w-full">
                        <img className='w-32 h-32 object-cover' src={`${transaction.Product.image}`} alt="order" />
                        <div className='ml-4 py-2 w-3/5'>
                          <p className='text-xl font-semibold truncate'>{transaction.Product.name}</p>
                          <p className='mt-2 capitalize'>Type: {transaction.Product.type}</p>
                          <p >Price: IDR {transaction.Product.price}</p>
                          <p>Qty: x{transaction.quantity}</p>
                        </div>
                        <div className='ml-4 py-2'>
                          <p className='text-xl font-semibold'>Total Purchases</p>
                          <p className='mt-2'>IDR {transaction.quantity * transaction.Product.price}</p>
                          <p className='font-semibold'>Status: Success</p>
                        </div>
                      </div>
                      <div className="flex justify-end">
                        <Link to={`/product/${transaction.Product.id}/detail`} className='bg-slate-800 py-2 px-4 rounded-sm text-white hover:opacity-80'>
                          Buy Again
                        </Link>
                      </div>
                    </div>
                  )
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="mb-64"></div>
      <Footer />
    </>
  )
}
