import axios from 'axios'
import React, { useContext, useState } from 'react'
import Footer from '../Components/Footer'
import UserSideBar from '../Components/UserSideBar'
import { UserContext } from '../Context/UserContext'

export default function UserChangePassword() {
  const api = 'https://ecommerce-app-rnp.herokuapp.com'
  const [user, setUser] = useContext(UserContext)

  const formInput = {
    "current_password": "",
    "new_password": ""
  }

  const [input, setInput] = useState(formInput)


  const handleInputChange = (event) => {
    const { name, value } = event.target
    setInput({ ...input, [name]: value })
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    try {
      await axios.patch(`${api}/users/change-password`, input, {headers: { Authorization: `Bearer ${user.token}` }})
      setUser(null)
      alert('Password updated')
    } catch (error) {
      alert(error.response.data.message)
    }
  }

  return (
    <>
      <div className="container">
        <div className='relative px-8 top-32'>
          <div className='flex justify-center mb-10'>
            <h1 className='font-medium text-5xl'>USER PROFILE</h1>
          </div>
          <div className="flex">
            <div className="w-1/5">
              <UserSideBar />
            </div>
            <div className="w-4/5 p-5 shadow-sm bg-white">
              <form onSubmit={handleSubmit}>
                <div className="form-group mb-6">
                  <label htmlFor="name" className="form-label inline-block mb-2 text-slate-900">CURRENT PASSWORD</label>
                  <input type="password" name='current_password' value={input.name} onChange={handleInputChange} className="form-control w-full px-3 py-1.5 border border-solid border-gray-300rounded focus:bg-white focus:border-blue-600 focus:outline-none" id="name"placeholder="Enter current password" />
                </div>
                <div className="form-group mb-6">
                  <label htmlFor="email" className="form-label inline-block mb-2 text-slate-900">NEW PASSWORD</label>
                  <input type="password" name='new_password' value={input.email} onChange={handleInputChange} className="form-control w-full px-3 py-1.5 border border-solid border-gray-300rounded focus:bg-white focus:border-blue-600 focus:outline-none" id="email"placeholder="Enter new password"/>
                </div>
                <button type="submit" className="px-6 py-2.5 bg-slate-900 text-white font-medium text-xs uppercase rounded shadow-md">Update</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div className="mb-64"></div>
      <Footer />
    </>
  )
}
