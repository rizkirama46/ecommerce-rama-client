import axios from 'axios'
import React, { useContext, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import Footer from '../Components/Footer'
import { UserContext } from '../Context/UserContext'

export default function Login() {
  const history = useNavigate()
  const api = `https://ecommerce-app-rnp.herokuapp.com/login`
  const formInput = {
    "email": "",
    "password": ""
  }

  const [, setUser] = useContext(UserContext)

  const [input, setInput] = useState(formInput)

  const handleInputChange = (event) => {
    const { name, value } = event.target
    setInput({ ...input, [name]: value })
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    try {
      const post = await axios.post(api, input)
      const currentUser = post.data.user
      currentUser.token = post.data.token
      setUser(currentUser)
      localStorage.setItem('user', JSON.stringify(currentUser))
      if(currentUser.role === 'admin') {
        return history('/admin/dashboard')
      }
      history('/')
    } catch (error) {
      alert(error.response.data.message)
    }
  }

  return (
    <>
      <div className="flex justify-center mt-36 pt-16 pb-52">
        <div className="w-full md:w-1/3 flex flex-col items-center">
          <h1 className='font-semibold text-xl'>LOGIN</h1>
          <p className='text-md my-5'>Please enter your e-mail and password</p>
          <form onSubmit={handleSubmit}>
            <input type="email" name='email' placeholder='Email' className='w-full border p-2 my-2' onChange={handleInputChange} />
            <input type="password" name='password' placeholder='Password' className='w-full border p-2 my-2' onChange={handleInputChange} />
            <button type='submit' className='w-full bg-slate-900 text-white py-3 my-2 hover:text-slate-900 hover:bg-white hover:border hover:border-slate-900 transition duration-500'>LOGIN</button>
          </form>
          <p className='text-slate-500 my-2'>Don't have an account? <Link to="/register" className='text-slate-900 font-semibold'>Create one</Link> </p>
        </div>
      </div>
      <Footer />
    </>
  )
}
