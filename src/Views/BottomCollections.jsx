import React from 'react'
import BaseCollection from '../Components/Collection/BaseCollection'

export default function BottomCollections() {
  return (
    <div>
      <BaseCollection 
        page="bottom" 
        title="BOTTOM COLLECTIONS" 
        api="https://ecommerce-app-rnp.herokuapp.com/products/bottom"/>
    </div>
  )
}
