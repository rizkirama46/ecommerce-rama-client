import axios from 'axios'
import React from 'react'
import { useState } from 'react'
import { useEffect } from 'react'
import { useContext } from 'react'
import Footer from '../Components/Footer'
import UserSideBar from '../Components/UserSideBar'
import { UserContext } from '../Context/UserContext'

export default function UserBalance() {
  const api = 'https://ecommerce-app-rnp.herokuapp.com'
  const [user, setUser] = useContext(UserContext)
  const [input, setInput] = useState({ "balance": 0, "type": 1})
  const [wallets, setWallets] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      const {data} = await axios.get(`${api}/wallets`, {headers: { Authorization: `Bearer ${user.token}` }})
      setWallets(data.data.DetailWallet)
    }
    fetchData()
  }, [api, user])

  const handleInputChange = (event) => {
    const { name, value } = event.target
    setInput({ ...input, [name]: value })
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    try {
      input.balance = parseFloat(input.balance)
      const {data} = await axios.put(`${api}/wallets`, input, {headers: { Authorization: `Bearer ${user.token}`}})
      setUser({ ...user, balance: parseFloat(user.balance) + parseFloat(input.balance) })
      alert(data.message)
    } catch (error) {
      alert(error.response.data.message)
    }
  }

  return (
    <>
      <div className="container">
        <div className='relative px-8 top-32'>
          <div className='flex justify-center mb-10'>
            <h1 className='font-medium text-5xl'>USER BALANCE</h1>
          </div>
          <div className="flex">
            <div className="w-1/5">
              <UserSideBar />
            </div>
            <div className="flex w-4/5 p-5 shadow-sm bg-white">
              <div className="w-2/5 border p-4 rounded-sm shadow-md mr-4">
                <p className='font-semibold text-gray-600'>Total Balance</p>
                <p className='text-xl font-bold text-slate-800'>Rp {parseFloat(user.balance)} </p>
                <form  className='flex mt-4'>
                  <input type="text" name='balance' className='border p-2 w-full' onChange={handleInputChange}/>
                  <button onClick={handleSubmit} className='px-4 py-2 ml-2 bg-slate-800 text-slate-200 rounded-md hover:opacity-80'>TOPUP</button>
                </form>
              </div>
              <div className='w-3/5 border rounded-sm shadow-md'>
                <h3 className='font-semibold text-xl py-2 px-4'>Balance History</h3>
                {wallets.map((wallet) => {
                  return (
                    <div key={wallet.detail_wallet_id} className="flex items-center border-t-2 p-2 shadow-sm rounded-sm">
                      <svg xmlns="http://www.w3.org/2000/svg" className="h-8 w-8" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M12 8c-1.657 0-3 .895-3 2s1.343 2 3 2 3 .895 3 2-1.343 2-3 2m0-8c1.11 0 2.08.402 2.599 1M12 8V7m0 1v8m0 0v1m0-1c-1.11 0-2.08-.402-2.599-1M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                      </svg>
                      <div className="flex justify-between w-full ml-5">
                        <p className='font-semibold'>{wallet.type === 1 ? "TopUp" : "Payment"}</p>
                        <p className='font-semibold'>IDR {wallet.balance}</p>
                      </div>
                    </div>
                  )
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="mb-64"></div>
      <Footer />
    </>
  )
}
