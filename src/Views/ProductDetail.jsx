import axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import Footer from '../Components/Footer'
import { CartContext } from '../Context/CartContext'
import { UserContext } from '../Context/UserContext'

export default function ProductDetail() {
  const { id } = useParams()
  const api = `https://ecommerce-app-rnp.herokuapp.com`
  const history = useNavigate()
  const [product, setProduct] = useState({})
  const [quantity, setQuantity] = useState(1)
  const [user] = useContext(UserContext)
  const [cart, setCart] = useContext(CartContext)
  
  const initialInput = {
    "product_id": parseInt(id),
    "quantity": 1
  }
  const [input, setInput] = useState(initialInput)

  useEffect(() => {
    const fetchData = async () => {
      if(id) {
        const response = await axios.get(`${api}/products/${id}`)
        setProduct(response.data.data)
      }
    }
    
    fetchData()
  }, [id, api])

  const plusQuantity = (event) => {
    event.preventDefault()
    if(quantity < product.stock) {
      setQuantity(quantity + 1)
      setInput({ ...input, quantity: quantity + 1 })
    }
  }

  const minusQuantity = (event) => {
    event.preventDefault()
    if (quantity === 1) {
      return setQuantity(1)
    }
    setQuantity(quantity - 1)
    setInput({ ...input, quantity: quantity - 1 })
  }


  const handleInputChange = (event) => {
    const { name, value } = event.target
    setInput({ ...input, [name]: value })
  }

  const addToCart = async (event) => {
    event.preventDefault()
    if (!user) {
      alert('Please login first')
      history('/login')
      return
    }
    const response = await axios.post(`${api}/carts`, input, {headers: {Authorization: `Bearer ${user.token}`}})
    setCart([...cart, response.data.data])
    alert('Successful add to cart')
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    try {
      if (!user) {
        alert('Please login first')
        history('/login')
        return
      }
      const response = await axios.post(`${api}/carts`, input, {headers: {Authorization: `Bearer ${user.token}`}})
      setCart([...cart, response.data.data])
      history("/user/cart")
    } catch (error) {
      alert(JSON.stringify(error.response.data))
    }
  }

  return (
    <>
      <div className="container my-32">
        <div className="flex">
          <div className='w-3/5 h-[700px]'>
            <img className='object-cover object-center h-full w-full' src={product.image_url} alt="product-detail" />
          </div>
          <div className='w-2/5 h-[700px] px-12 py-4'>
            <form onSubmit={handleSubmit}>
              <h1 className='font-semibold text-2xl'>{product.product_name}</h1>
              <p className='font-semibold text-xl text-slate-500 mt-5'>IDR {product.price}</p>
              <h2 className='font-semibold text-xl mt-5'>Description</h2>
              <p className='mt-2'>{product.description}</p>
              <h2 className='font-semibold text-xl mt-5'>Quantity : </h2>
              <div className="flex mt-2 items-center">
                <button onClick={(e)=> minusQuantity(e)} className='w-1/6 border bg-slate-50 my-2'>
                  <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 m-auto" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                    <path strokeLinecap="round" strokeLinejoin="round" d="M18 12H6" />
                  </svg>
                </button>
                <input type="text" name='quantity' value={quantity} className='w-1/6 px-6 focus:outline-none' onChange={handleInputChange}/>
                <button onClick={(e)=> plusQuantity(e)} className='w-1/6 border bg-slate-50 my-2'>
                  <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 m-auto" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                    <path strokeLinecap="round" strokeLinejoin="round" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                  </svg>
                </button>
                <p className='ml-3 text-slate-500 text-sm'>only {product.stock} pieces left </p>
              </div>
              <button onClick={(e) => addToCart(e)} className='w-full py-4  mt-5 text-slate-900 border border-slate-900  hover:bg-slate-900 hover:text-white transition duration-500'>ADD TO CART - IDR {product.price}</button>
              <button type='submit' className='w-full py-4 bg-slate-900 text-white mt-5 hover:text-slate-900 hover:bg-white hover:border hover:border-slate-900 transition duration-500'>BUY IT NOW</button>
            </form>
          </div>
        </div>
      </div>
      <Footer />
    </>
  )
}
