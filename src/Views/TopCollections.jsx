import React from 'react'
import BaseCollection from '../Components/Collection/BaseCollection'

export default function TopCollections() {
  return (
    <div>
      <BaseCollection 
        page="top" 
        title="TOP COLLECTIONS" 
        api="https://ecommerce-app-rnp.herokuapp.com/products/top"/>
    </div>
  )
}
