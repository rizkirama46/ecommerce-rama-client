import React from 'react'
import BaseCollection from '../Components/Collection/BaseCollection'

export default function Collections() {
  return (
    <div>
      <BaseCollection  
        title="COLLECTIONS" 
        api="https://ecommerce-app-rnp.herokuapp.com/products"/>
    </div>
  )
}
