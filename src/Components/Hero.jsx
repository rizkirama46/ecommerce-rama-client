import React from 'react'

import { Swiper, SwiperSlide } from "swiper/react";

// import required modules
import { Autoplay, Pagination, Navigation } from "swiper";

export default function Hero() {
  return (
    <>
      <Swiper
        spaceBetween={30}
        centeredSlides={true}
        autoplay={{
          delay: 10000,
          disableOnInteraction: false,
        }}
        pagination={{
          clickable: true,
        }}
        modules={[Autoplay, Pagination, Navigation]}
        className="mySwiper"
      >
        <SwiperSlide>
          <div className='h-[500px] lg:h-[600px] bg-center bg-cover bg-[url("https://images.unsplash.com/photo-1621018987522-73cf1cf9ae8a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80")]'>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className='h-[500px] lg:h-[600px] bg-center bg-cover bg-[url("https://images.unsplash.com/photo-1441986300917-64674bd600d8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80")]'>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className='h-[500px] lg:h-[600px] bg-center bg-cover bg-[url("https://images.unsplash.com/photo-1612423284934-2850a4ea6b0f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80")]'>
          </div>
        </SwiperSlide>
      </Swiper>
    </>
  )
}
