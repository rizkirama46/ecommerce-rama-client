import React, { useEffect, useState } from 'react'
import styles from './HomeBestSeller.module.css'

// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

// import required modules
import { Pagination, Navigation } from "swiper";
import axios from 'axios';
import { Link } from 'react-router-dom';
import SkeletonsHomeBestSeller from './Skeletons/SkeletonsHomeBestSeller';

export default function BestSeller() {
  const api = 'https://ecommerce-app-rnp.herokuapp.com'
  const [products, setProducts] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      const response = await axios.get(`${api}/products`)

      setProducts(response.data.data.map(product => {
        let sold = 0
        
        if(product.Cart.length >= 1) {
          product.Cart.forEach( t => {
            sold += t.quantity
          })
        }

        return {
          id: product.product_id,
          name: product.product_name,
          price: product.price,
          image: product.image_url,
          category: product.category_id,
          description: product.description,
          sold: sold
        }
      }))

    }

    fetchData()
  }, [])

  products.sort((a, b) => b.sold - a.sold)

  return (
    <div className="container my-20 md:px-20">
      <div>
        <div className='flex justify-center'>
          <h1 className='text-3xl font-semibold'>Best Seller</h1>
        </div>
        <div className="mt-16 ">
          {products.length <= 0 && (
            <SkeletonsHomeBestSeller />
          )}
          <Swiper
            slidesPerView={4}
            spaceBetween={40}
            slidesPerGroup={4}
            loop={true}
            loopFillGroupWithBlank={true}
            navigation={true}
            modules={[Pagination, Navigation]}
            className="mySwiper"
          >
            { products.map(product => {
              return (
                <SwiperSlide key={product.id}>
                  <Link to={`/product/${product.id}/detail`}>
                    <img className={styles.cardImage} src={`${product.image}`} alt="bestseller" />
                    <div className={styles.cardContent}>
                      <p className={styles.cardTitle}>{product.name}</p>
                      <p className={styles.cardPrice}>IDR {product.price}</p>
                    </div>
                  </Link>
                </SwiperSlide>
              )
            })}
          </Swiper>
        </div>
      </div>
    </div>
  )
}
