import React from 'react'

export default function SkeletonsCollection() {
  return (
    <>
      <div className="shadow rounded-md w-full mb-2 md:w-1/3 lg:w-1/5">
        <div className="animate-pulse">
          <div className="bg-slate-200 h-80 md:h-56 w-full"></div>
          <div className='px-1 py-3 space-y-3'>
            <div className="h-2 bg-slate-200 rounded"></div>
            <div className="h-2 bg-slate-200 rounded"></div>
          </div>
        </div>
      </div>
      <div className="shadow rounded-md w-full mb-2 md:w-1/3 lg:w-1/5">
        <div className="animate-pulse">
          <div className="bg-slate-200 h-80 md:h-56 w-full"></div>
          <div className='px-1 py-3 space-y-3'>
            <div className="h-2 bg-slate-200 rounded"></div>
            <div className="h-2 bg-slate-200 rounded"></div>
          </div>
        </div>
      </div>
      <div className="shadow rounded-md w-full mb-2 md:w-1/3 lg:w-1/5">
        <div className="animate-pulse">
          <div className="bg-slate-200 h-80 md:h-56 w-full"></div>
          <div className='px-1 py-3 space-y-3'>
            <div className="h-2 bg-slate-200 rounded"></div>
            <div className="h-2 bg-slate-200 rounded"></div>
          </div>
        </div>
      </div>
      <div className="shadow rounded-md w-full mb-2 md:w-1/3 lg:w-1/5">
        <div className="animate-pulse">
          <div className="bg-slate-200 h-80 md:h-56 w-full"></div>
          <div className='px-1 py-3 space-y-3'>
            <div className="h-2 bg-slate-200 rounded"></div>
            <div className="h-2 bg-slate-200 rounded"></div>
          </div>
        </div>
      </div>
    </>
  )
}
