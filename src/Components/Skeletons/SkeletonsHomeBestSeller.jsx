import React from 'react'

export default function SkeletonsHomeBestSeller() {
  return (
    <div className='flex gap-x-5 justify-between'>
      <div className="shadow rounded-md w-full mb-2 md:w-1/4">
        <div className="animate-pulse">
          <div className="bg-slate-200 h-80 w-full"></div>
          <div className='px-1 py-3 space-y-3'>
            <div className="h-2 bg-slate-200 rounded"></div>
            <div className="h-2 bg-slate-200 rounded"></div>
          </div>
        </div>
      </div>
      <div className="shadow rounded-md w-full mb-2 md:w-1/4">
        <div className="animate-pulse">
          <div className="bg-slate-200 h-80 w-full"></div>
          <div className='px-1 py-3 space-y-3'>
            <div className="h-2 bg-slate-200 rounded"></div>
            <div className="h-2 bg-slate-200 rounded"></div>
          </div>
        </div>
      </div>
      <div className="shadow rounded-md w-full mb-2 md:w-1/4">
        <div className="animate-pulse">
          <div className="bg-slate-200 h-80 w-full"></div>
          <div className='px-1 py-3 space-y-3'>
            <div className="h-2 bg-slate-200 rounded"></div>
            <div className="h-2 bg-slate-200 rounded"></div>
          </div>
        </div>
      </div>
      <div className="shadow rounded-md w-full mb-2 md:w-1/4">
        <div className="animate-pulse">
          <div className="bg-slate-200 h-80 w-full"></div>
          <div className='px-1 py-3 space-y-3'>
            <div className="h-2 bg-slate-200 rounded"></div>
            <div className="h-2 bg-slate-200 rounded"></div>
          </div>
        </div>
      </div>
    </div>
  )
}
