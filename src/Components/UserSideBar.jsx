import React from 'react'
import { Link } from 'react-router-dom'

export default function UserSideBar() {
  return (
    <div className="flex flex-col items-center px-8">
    <img className='h-40 w-full object-cover' src="https://sekolahutsman.sch.id/wp-content/uploads/2016/03/no-profile.png" alt="profile"/>
    <p className='font-semibold text-md mt-2'>USER DATA</p>
    <div className='my-2'>
      <ul className='text-sm flex flex-col gap-1'>
        <li className='hover:text-slate-500'><Link to="/user/profile">My Profile</Link></li>
        <li className='hover:text-slate-500'><Link to="/user/order">History Order</Link></li>
        <li className='hover:text-slate-500'><Link to="/user/balance">Balance</Link></li>
        <li className='hover:text-slate-500'><Link to="/user/change-password">Change Password</Link></li>
      </ul>
    </div>
  </div>
  )
}
