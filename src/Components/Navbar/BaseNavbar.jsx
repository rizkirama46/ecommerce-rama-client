import React, { useContext, useState } from 'react'
import { Link } from "react-router-dom";
import { CartContext } from '../../Context/CartContext';
import { UserContext } from '../../Context/UserContext';
import UserNavbar from './UserNavbar';

export default function Navbar() {
  const [user, setUser] = useContext(UserContext)
  const [, setCart] = useContext(CartContext)
  const [open, setOpen] = useState(false);

  return (
    <>
      <div className='fixed bg-gradient-to-br from-gray-900 via-slate-800 to-gray-900 top-0 left-0 right-0 z-10'>
        <div className='border-b border-white/10 py-3'>
          <div className='container'>
            <nav className='flex justify-between items-center px-4 py-2.5 md:py-2 relative'>
              <div className='flex items-center gap-x-8'>
                <Link to="/" className="text-white font-semibold uppercase mr-6">RNP OFFICIAL</Link>
                { user?.role !== 'admin' && (
                  <>
                    <Link to="/collections" className="hidden md:block text-gray-300 hover:text-white hover:bg-gray-700/40 rounded-lg">Collections</Link>
                  </>
                )}
              </div>
              <button onClick={()=> setOpen(!open)} className='focus:outline-none'>
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-white md:hidden" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                  <path strokeLinecap="round" strokeLinejoin="round" d="M4 6h16M4 12h16M4 18h16" />
                </svg>
              </button>
              <div className={`${open ? 'block' : 'hidden'} md:hidden bg-white absolute top-0 right-0 mt-8 mr-14 py-2 rounded-lg w-44 overflow-hidden`}>
                <Link to="/collections" className="px-4 py-2 text-sm hover:bg-gray-100 hover:text-black block">Collections</Link>
                <Link to="/products/best-seller" className="px-4 py-2 text-sm hover:bg-gray-100 hover:text-black block">Best Seller</Link>
                <div className='w-full h-px bg-gray-300 my-1' />
                <Link to="/products/top" className="px-4 py-2 text-sm hover:bg-gray-100 hover:text-black block">Top</Link>
                <Link to="/products/bottom" className="px-4 py-2 text-sm hover:bg-gray-100 hover:text-black block">Bottom</Link>
                <Link to="/products/accessories" className="px-4 py-2 text-sm hover:bg-gray-100 hover:text-black block">Accessories</Link>
                <div className='w-full h-px bg-gray-300 my-1' />
                { !user && (
                  <>
                    <Link to="/login" className="px-4 py-2 text-sm hover:bg-gray-100 hover:text-black block">Sign In</Link>
                    <Link to="/register" className="px-4 py-2 text-sm hover:bg-gray-100 hover:text-black block">Sign Up</Link>
                  </>
                )}
                { user && (
                  <>
                    <Link to="/user/profile" className="px-4 py-2 text-sm hover:bg-gray-100 hover:text-black block">Profile</Link>
                    <Link to="/user/cart" className="px-4 py-2 text-sm hover:bg-gray-100 hover:text-black block">My Cart</Link>
                    <Link to="/user/order" className="px-4 py-2 text-sm hover:bg-gray-100 hover:text-black block">My Orders</Link>
                    <button className="px-4 py-2 text-sm hover:bg-gray-100 hover:text-black block" onClick={()=> {
                      setOpen(!open)
                      setUser(null)
                      setCart(null)
                      localStorage.removeItem('user')
                    }}>Logout</button>
                  </>
                )}
              </div>
              <div className="hidden md:flex items-center gap-x-8">
                { !user && (
                    <>
                      <Link to="/login" className="text-gray-300 hover:text-white hover:bg-gray-700/40 rounded-lg">Sign in</Link>
                      <Link to="/register" className="text-gray-300 hover:text-white hover:bg-gray-700/40 rounded-lg">Sign up</Link>
                    </>
                  )
                }
                { user && (
                  <UserNavbar />
                )}
              </div>
            </nav>
          </div>
        </div>
      </div>
    </>
  )
}
