import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { CartContext } from '../../Context/CartContext';
import { UserContext } from '../../Context/UserContext';

export default function UserCart() {
  const api = 'https://ecommerce-app-rnp.herokuapp.com';
  const [open, setOpen] = useState(false);
  const [user] = useContext(UserContext)
  const [cart, setCart] = useContext(CartContext)

  useEffect(() => {
    const fetchData = async () => {
      if (user) {
        const response = await axios.get(`${api}/carts`, {headers: {'Authorization': `Bearer ${user.token}`}})

        setCart(response.data.data.map(cart =>{
          return cart 
        })) 
      }
    }

    fetchData()
  }, [user, api, setCart])
  
  return (
    <>
      <button onClick={() => setOpen(!open) } className="relative">
        <svg xmlns="http://www.w3.org/2000/svg" className="h-7 w-7 text-gray-300 " viewBox="0 0 20 20" fill="currentColor">
          <path d="M3 1a1 1 0 000 2h1.22l.305 1.222a.997.997 0 00.01.042l1.358 5.43-.893.892C3.74 11.846 4.632 14 6.414 14H15a1 1 0 000-2H6.414l1-1H14a1 1 0 00.894-.553l3-6A1 1 0 0017 3H6.28l-.31-1.243A1 1 0 005 1H3zM16 16.5a1.5 1.5 0 11-3 0 1.5 1.5 0 013 0zM6.5 18a1.5 1.5 0 100-3 1.5 1.5 0 000 3z" />
        </svg>
        <div className="absolute bg-gray-300 -top-3 -right-3 px-1 rounded-md">
          <p className='text-slate-800'>{cart?.length}</p>
        </div>
      </button>

      { open && (
        <div className='w-1/4 bg-white absolute text-sm shadow-md rounded-md right-36 top-10'>
          <div className='flex items-center justify-center p-4 border-b-2'>
            <h2 className='font-semibold'>CART</h2>
          </div>
          <ul className='flex flex-col gap-3 p-4'>
            { cart?.map(item => {
              return (
                <li className='hover:text-slate-500' key={item.id}>
                  <Link to="/user/order" className='flex'>
                    <img className='h-10 w-10 object-cover' src={`${item.Product.image_url}`} alt="cart" />
                    <p className='ml-2 truncate w-full'>{item.Product.product_name}</p>
                    <p className='ml-3 text-slate-800 font-semibold'>{item.Product.price}</p>
                  </Link>
                </li>
              )
            })}
          </ul>
          <div className='flex items-center justify-end p-3 border-t-2'>
            <Link to="/user/cart" className='font-semibold bg-slate-800 py-2 px-4 text-white hover:opacity-80'>view shopping cart</Link>
          </div>
        </div>
      )}
    </>
  )
}
