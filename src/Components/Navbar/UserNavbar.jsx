import React, { useContext, useState } from 'react'
import { Link } from 'react-router-dom';
import { CartContext } from '../../Context/CartContext';
import { UserContext } from '../../Context/UserContext';
import UserCart from './UserCart';

export default function UserNavbar() {
  const [open, setOpen] = useState(false);
  const [, setCart] = useContext(CartContext)
  const [user, setUser] = useContext(UserContext)

  return (
    <>
      <div className='flex gap-6 items-center'>
        { user?.role !== 'admin' && (
          <UserCart />

        )}
        <div className="relative">
          <button onClick={() => {
            setOpen(!open)
          }} className='text-gray-300 flex items-center'>
            <p className='text-base capitalize'>{user.username.split(' ')[0]}</p>
            <svg xmlns="http://www.w3.org/2000/svg" className={`${open ? 'rotate-180' : ''} ml-1 h-5 w-5 text-gray-300 transition duration-300`} fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
              <path strokeLinecap="round" strokeLinejoin="round" d="M19 9l-7 7-7-7" />
            </svg>
          </button>
          { open && (
            <div className='bg-white absolute text-sm px-4 py-4 shadow-sm whitespace-nowrap right-0 top-8 transition-opacity duration-500' >
              <ul className='flex flex-col gap-3'>
              { user?.role !== 'admin' && (
                <>
                  <li className='hover:text-slate-500'><Link to="/user/profile">My Profile</Link></li>
                  <li className='hover:text-slate-500'><Link to="/user/order">My Orders</Link></li>
                </>
              )}
                <li className='hover:text-slate-500'><button onClick={()=> {
                      setOpen(!open)
                      setUser(null)
                      setCart(null)
                      localStorage.removeItem('user')
                    }}>Logout</button>
                </li>
              </ul>
            </div>
          )}
        </div>
      </div>
    </>
  )
}
