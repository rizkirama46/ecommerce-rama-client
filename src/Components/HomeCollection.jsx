import React from 'react'
import { Link } from 'react-router-dom'
import styles from './HomeCollection.module.css'

export default function HomeCollection() {
  return (
    <div className="container my-20">
      <div className='flex justify-center'>
        <Link to="/collections" className='bg-slate-900 text-white px-8 py-2 hover:text-slate-900 hover:bg-white hover:border hover:border-slate-900 transition duration-500'>VIEW COLLECTION</Link>
      </div>
      <div className="flex flex-wrap md:flex-nowrap gap-6 mt-16 md:px-8">
        <Link to ="/collections/top" className={styles.card}>
          <div className={`${styles.cardContent} bg-[url("https://images.unsplash.com/photo-1517255996494-6cc16ba8230c?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=60&raw_url=true&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8Z2lybCUyMHNoaXJ0fGVufDB8MXwwfGJsYWNrfA%3D%3D&auto=format&fit=crop&w=500")]`}>
            <h2 className={styles.cardTitle}>tops</h2>
            <Link to="/collections/top" className={styles.cardButton}>VIEW PRODUCTS</Link>
          </div>
        </Link>
        <Link to ="/collections/bottom" className={styles.card}>
          <div className={`${styles.cardContent} bg-[url("https://images.unsplash.com/photo-1548883354-7622d03aca27?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=60&raw_url=true&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8cGFudHN8ZW58MHwxfDB8YmxhY2t8&auto=format&fit=crop&w=500")]`}>
            <h2 className={styles.cardTitle}>bottoms</h2>
            <Link to="/collections/bottom" className={styles.cardButton}>VIEW PRODUCTS</Link>
          </div>
        </Link>
        <Link to ="/collections/accessories" className={styles.card}>
          <div className={`${styles.cardContent} bg-[url("https://images.unsplash.com/photo-1612011213611-ff46ab689475?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=60&raw_url=true&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8bmVja2xhY2VzfGVufDB8MXwwfGJsYWNrfA%3D%3D&auto=format&fit=crop&w=500")]`}>
            <h2 className={styles.cardTitle}>accessories</h2>
            <Link to="/collections/accessories" className={styles.cardButton}>VIEW PRODUCTS</Link>
          </div>
        </Link>
      </div>
    </div>
  )
}
