import axios from 'axios'
import React, { useEffect, useState } from 'react'
import Footer from '../Footer'
import CategoryCollection from './CategoryCollection'
import SortCollection from './SortCollection'
import ContentCollection from './ContentCollection'
import SkeletonsCollection from '../Skeletons/SkeletonsCollection'

export default function BaseCollection({ page, title, api }) {
  const [products, setProducts] = useState([])
  const [filter, setFilter] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      const resProducts = await axios.get(api)

      setProducts(resProducts.data.data.map(product => {
        let sold = 0
        
        if(product.Cart?.length >= 1) {
          product.Cart.forEach( t => {
            sold += t.quantity
          })
        }

        return {
          id: product.product_id,
          name: product.product_name,
          price: product.price,
          image: product.image_url,
          category: product.category_id,
          description: product.description,
          sold: sold
        }
      }))

      setFilter(resProducts.data.data.map(product => {
        let sold = 0
        
        if(product.Cart?.length >= 1) {
          product.Cart.forEach( t => {
            sold += t.quantity
          })
        }

        return {
          id: product.product_id,
          name: product.product_name,
          price: product.price,
          image: product.image_url,
          category: product.category_id,
          description: product.description,
          sold: sold
        }
      }))
    }

    fetchData()
  }, [api])

  return (
    <div className='relative top-32'>
      <div className='flex justify-center mb-10'>
        <h1 className='font-medium text-5xl'>{title}</h1>
      </div>
      <SortCollection data={[filter, setFilter]} />
      <div className="container my-12">
        <div className="flex px-2 md:px-8">
          <div className='hidden w-1/6 md:block'>
            <CategoryCollection products={products} data={[filter, setFilter]} page={page} />
          </div>
          <div className='w-full md:w-5/6'>
            {products.length <= 0 && (
              <div className='flex flex-wrap gap-2 md:gap-x-12 md:gap-y-8'>
                <SkeletonsCollection />
              </div>
            )}
            <ContentCollection data={[filter, setFilter]} />
          </div>
        </div>
      </div>
      <div className="pb-16"></div>
      <Footer />
    </div>
  )
}
