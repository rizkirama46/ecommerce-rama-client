import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

export default function CategoryCollection({ products, data: [, setFilter], page }) {
  const api = 'https://ecommerce-app-rnp.herokuapp.com/categories'
  const [categories, setCategories] = useState([])
  
  useEffect(() => {
    const fetchData = async () => {
      const resCategories = await axios.get(api)
      let arrCategories = []

      if(page) {
        resCategories.data.data.forEach(category => {
          if(category.Product[0].type === page) {
            arrCategories.push({
              id: category.category_id,
              name: category.category_name,
            })
          }
        })
      } else {
        resCategories.data.data.forEach(category => {
          arrCategories.push({
            id: category.category_id,
            name: category.category_name,
          })
        }) 
      }

      setCategories(arrCategories)
    }

    fetchData()
  }, [page])

  const handleFilter = (value) => {
    let filtered = []
    const data = [...products]

    if(value === 'collections') {
      return setFilter(data)
    }
    filtered = data.filter(product => product.category === value)
    
    return setFilter(filtered)
  }

  return (
    <>
      <div>
        <h2 className='font-medium mb-2'>CATEGORY</h2>
        <button onClick={() => handleFilter("collections")} className='mb-1 hover:underline hover:opacity-80'>Collections</button>
        <ul className='flex flex-col gap-y-1'>
          {categories.map(category => {
            return (
              <li key={category.id}><button onClick={() => handleFilter(category.id)} className='hover:underline hover:opacity-80'>{category.name}</button></li>
            )
          })}
        </ul>
      </div>
      <div>
        <h2 className='font-medium mt-4 mb-2'>TYPE</h2>
        <ul className='flex flex-col gap-y-1'>
          <li className='hover:underline hover:opacity-80'><Link to="/collections/top">Tops</Link></li>
          <li className='hover:underline hover:opacity-80'><Link to="/collections/bottom">Bottoms</Link></li>
          <li className='hover:underline hover:opacity-80'><Link to="/collections/accessories">Accessories</Link></li>
        </ul>
      </div>
    </>
  )
}
