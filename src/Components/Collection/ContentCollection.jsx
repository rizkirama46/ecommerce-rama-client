import React from 'react'
import { Link } from 'react-router-dom'
import styles from './ContentCollection.module.css'

export default function ContentCollection({ data:[filter] }) {
  return (
    <div className='flex flex-wrap gap-2 md:gap-x-12 md:gap-y-8'>
      {filter.map(product => {
        return (
          <Link to={`/product/${product.id}/detail`} className='w-full mb-2 md:w-1/3 lg:w-1/5 md:mb-0 transition delay-1000' key={product.id}>
            <img className={styles.cardImage} src={product.image} alt="bestseller" />
            <div className={styles.cardContent}>
              <p className={styles.cardTitle}>{product.name}</p>
              <p className={styles.cardPrice}>IDR {product.price}</p>
            </div>
          </Link>
        )
      })}
    </div>
  )
}
