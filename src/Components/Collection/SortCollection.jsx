import React, { useState } from 'react'

export default function SortCollection({ data:[filter, setFilter] }) {
  const [sortOpen, setSortOpen] = useState(false)

  const handleSort = (value) => {
    let filtered = []
    const data = [...filter]

    if(value === 'lowest') {
      filtered = data.sort((a, b) => a.price - b.price)
    }
    if(value === 'highest') {
      filtered = data.sort((a, b) => b.price - a.price)
    }
    if(value === 'best-selling') {
      filtered = data.sort((a, b) => b.sold - a.sold)
    }

    return setFilter(filtered)
  }

  return (
    <div className="flex border border-y-2 py-2">
    <div className="container relative">
      <div className="grid justify-items-end">
        <div>
          <button onClick={()=> setSortOpen(!sortOpen)} className='flex py-2 px-4 items-center'>
            SORT
            <svg xmlns="http://www.w3.org/2000/svg" className="ml-2 h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
              <path strokeLinecap="round" strokeLinejoin="round" d="M19 9l-7 7-7-7" />
            </svg>
          </button>
          <div className={`${sortOpen ? 'block' : 'hidden'} absolute bg-white border px-4 py-4 right-10 shadow-sm rounded-sm`}>
            <ul className='flex flex-col gap-2'>
              <li><button onClick={() => handleSort("best-selling")} className='hover:underline hover:opacity-80'>BEST SELLING</button></li>
              <li><button onClick={() => handleSort("lowest")} className='hover:underline hover:opacity-80'>PRICE, LOW TO HIGH</button></li>
              <li><button onClick={() => handleSort("highest")} className='hover:underline hover:opacity-80'>PRICE, HIGH TO LOW</button></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  )
}
