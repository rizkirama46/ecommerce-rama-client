import React from 'react'
import { Link } from 'react-router-dom'

export default function AdminSideBar() {
  return (
    <div className="fixed w-1/6 p-5 text-white top-0 left-0 bottom-0 z-10 bg-gradient-to-br from-gray-900 via-slate-800 to-gray-900">
      <div className='flex justify-center items-center'>
        <h1 className='text-xl font-semibold'>RNP OFFICIAL</h1>
      </div>
      <div className='mt-10 text-gray-300 px-4'>
        <ul className='flex flex-col gap-8'>
          <li>
            <Link to="/admin/dashboard" className='flex gap-x-5'>
              <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M2 10a8 8 0 018-8v8h8a8 8 0 11-16 0z"></path><path d="M12 2.252A8.014 8.014 0 0117.748 8H12V2.252z"></path></svg>
              <p>Dashboard</p>
            </Link>
          </li>
          <li>
            <Link to="/admin/products" className='flex gap-x-5'>
              <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M10 2a4 4 0 00-4 4v1H5a1 1 0 00-.994.89l-1 9A1 1 0 004 18h12a1 1 0 00.994-1.11l-1-9A1 1 0 0015 7h-1V6a4 4 0 00-4-4zm2 5V6a2 2 0 10-4 0v1h4zm-6 3a1 1 0 112 0 1 1 0 01-2 0zm7-1a1 1 0 100 2 1 1 0 000-2z" clipRule="evenodd"></path></svg>
              <p>Products</p>
            </Link>
          </li>
          <li>
            <Link to="/admin/categories" className='flex gap-x-5'>
            <svg className="flex-shrink-0 w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M5 3a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2V5a2 2 0 00-2-2H5zM5 11a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2v-2a2 2 0 00-2-2H5zM11 5a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V5zM11 13a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z"></path></svg>
              <p>Categories</p>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  )
}
