import { Routes, Route, BrowserRouter as Router, Navigate, Outlet } from "react-router-dom";
import Navbar from "../Components/Navbar/BaseNavbar";
import Home from '../Views/Home';
import Register from "../Views/Register";
import Login from "../Views/Login";
import BestSeller from "../Views/BestSeller";
import Collections from "../Views/Collections";
import ProductDetail from "../Views/ProductDetail";
import Order from "../Views/Order";
import { useContext } from "react";
import { UserContext } from "../Context/UserContext";
import TopCollections from "../Views/TopCollections";
import BottomCollections from "../Views/BottomCollections";
import AccessoriesCollections from "../Views/AccessoriesCollections";
import UserCart from "../Views/UserCart";
import UserProfile from "../Views/UserProfile";
import UserChangePassword from "../Views/UserChangePassword";
import Dashboard from "../Views/Admin/Dashboard";
import Products from "../Views/Admin/Products";
import FormProduct from "../Views/Admin/FormProduct";
import Categories from "../Views/Admin/Categories";
import FormCategory from "../Views/Admin/FormCategories";
import UserBalance from "../Views/UserBalance";

export default function Index() {
  const [user] = useContext(UserContext)

  const PrivateRoute = () =>{
    if (user){
      return <Outlet />
    }else{
      return <Navigate to="/login" />
    }
  }

  return (
    <Router>
      <Navbar />
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route exact path="/collections" element={<Collections/>} />
        <Route exact path="/collections/top" element={<TopCollections/>} />
        <Route exact path="/collections/bottom" element={<BottomCollections/>} />
        <Route exact path="/collections/accessories" element={<AccessoriesCollections/>} />
        <Route exact path="/products/best-seller" element={<BestSeller/>} />
        <Route exact path="/login" element={<Login/>} />
        <Route exact path="/register" element={<Register/>} />
        <Route exact path="/product/:id/detail" element={<ProductDetail/>} />
        
        <Route path="/user/order" element={<PrivateRoute />}>
          <Route path="/user/order" element={<Order/>} />
        </Route>
        <Route path="/user/cart" element={<PrivateRoute />}>
          <Route path="/user/cart" element={<UserCart/>} />
        </Route>
        <Route path="/user/profile" element={<PrivateRoute />}>
          <Route path="/user/profile" element={<UserProfile/>} />
        </Route>
        <Route path="/user/balance" element={<PrivateRoute />}>
          <Route path="/user/balance" element={<UserBalance/>} />
        </Route>
        <Route path="/user/change-password" element={<PrivateRoute />}>
          <Route path="/user/change-password" element={<UserChangePassword/>} />
        </Route>

        {/* admin */}
        <Route path="/admin/dashboard" element={<PrivateRoute />}>
          <Route path="/admin/dashboard" element={<Dashboard />} />
        </Route>
        <Route path="/admin/products" element={<PrivateRoute />}>
          <Route path="/admin/products" element={<Products />} />
        </Route>
        <Route path="/admin/products/create" element={<PrivateRoute />}>
          <Route path="/admin/products/create" element={<FormProduct />} />
        </Route>
        <Route path="/admin/product/:id/edit" element={<PrivateRoute />}>
          <Route path="/admin/product/:id/edit" element={<FormProduct />} />
        </Route>
        <Route path="/admin/categories" element={<PrivateRoute />}>
          <Route path="/admin/categories" element={<Categories />} />
        </Route>
        <Route path="/admin/category/create" element={<PrivateRoute />}>
          <Route path="/admin/category/create" element={<FormCategory />} />
        </Route>
        <Route path="/admin/category/:id/edit" element={<PrivateRoute />}>
          <Route path="/admin/category/:id/edit" element={<FormCategory />} />
        </Route>
      </Routes>
    </Router>
  )
}
